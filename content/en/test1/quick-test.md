---
title: Quick test
linktitle: Quick test
description: Create a Hugo site using the beautiful Ananke theme.
date: 2013-07-01
publishdate: 2013-07-01
categories: [getting test]
keywords: [quick test,usage]
authors: [Shekhar Gulati, Ryan Watters]
menu:
  test:
    parent: "test1"
    weight: 10
weight: 10
sections_weight: 10
draft: false
aliases: [/quicktest/,/overview/quicktest/]
toc: true
---

{{% note %}}
This quick start uses `macOS` in the examples. For instructions about how to install Hugo on other operating systems, see [install](/getting-started/installing).