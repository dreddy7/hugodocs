---
title: test Started
linktitle: test Started Overview
description: Quick start and guides for installing Hugo on your preferred operating system.
date: 2017-02-01
publishdate: 2017-02-01
lastmod: 2017-02-01
categories: [test started]
keywords: [usage,docs]
menu:
  test:
    parent: "test1"
    weight: 1
weight: 0001	#rem
draft: false
aliases: [/overvie/introductio/]
toc: false
---

If this is your first time using Hugo and you've [already installed Hugo on your machine][installed], we recommend the [quick start][].

[installed]: /getting-started/installing/
[quick start]: /getting-started/quick-start/